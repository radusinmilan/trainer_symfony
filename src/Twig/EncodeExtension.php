<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class EncodeExtension extends AbstractExtension
{

    public function getFilters()
    {
        return [
            new TwigFilter('test_up', [$this, 'testFilter'])
        ];
    }

    public function testFilter($value)
    {
        return strtoupper($value). '  ' . 'this is Twig extension';
    }
}