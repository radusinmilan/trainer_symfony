<?php


namespace App\Services;


use App\Entity\Trainer;
use App\Repository\TrainerRepository;

class TrainerService
{
    protected $trainerRepository;

    /**
     * TrainerService constructor.
     * @param $trainerRepository
     */
    public function __construct(TrainerRepository $trainerRepository)
    {
        $this->trainerRepository = $trainerRepository;
    }

    public function save(Trainer $trainer): void
    {
        $this->trainerRepository->persist($trainer);
        $this->trainerRepository->flush();
    }

    public function findAll()
    {
        return $this->trainerRepository->findAll();
    }

}