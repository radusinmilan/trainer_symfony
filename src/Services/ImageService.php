<?php


namespace App\Services;


use App\Entity\Image;
use App\Repository\ImageRepository;

class ImageService
{

    protected $imageRepository;

    /**
     * ImageService constructor.
     * @param $imageRepository
     */
    public function __construct(ImageRepository $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }

    /**
     * @param Image $image
     */
    public function saveImage(Image $image)
    {
        $this->imageRepository->persist($image);
        $this->imageRepository->flush();
    }

}