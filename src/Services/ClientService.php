<?php


namespace App\Services;


use App\Entity\Client;
use App\Repository\ClientRepository;

class ClientService
{

    protected $clientRepository;

    /**
     * ClientService constructor.
     * @param $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function findAll()
    {
       // dd($this->clientRepository->findAlll());
        return $this->clientRepository->findAll();
    }

    /**
     * @param Client $client
     * Save client in database
     */
    public function save(Client $client): void
    {
        $this->clientRepository->persist($client);
        $this->clientRepository->flush();
    }

    /**
     * @param  $id
     * Delete client in database
     * @throws \Exception
     */
    public function delete($id)
    {
        $client = $this->clientRepository->find($id);
        if(!$client) {
            throw new \Exception('error');
        }

        $this->clientRepository->delete($client);
    }

    /**
     * @param $id
     * @return Client
     * @throws \Exception
     */
    public function findById($id): Client
    {
        $client = $this->clientRepository->find($id);
        if(!$client) {
            throw new \Exception('error');
        }

        return $client;
    }

    /**
     * @param Client $client
     * @param $id
     * @throws \Exception
     */
    public function update(Client $client, $id)
    {
        $client = $this->findById($id);
        $this->clientRepository->persist($client);
        $this->clientRepository->flush();
    }

    /**
     * @param $search
     * @return Client[]|int|mixed|string
     */
    public function search($search)
    {
        return $this->clientRepository->search($search)
                ? $this->clientRepository->search($search)
                : $this->clientRepository->findAll();
    }
}