<?php

namespace App\Form;

use App\Entity\Trainer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrainerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    // 'required'=> true,
                    'attr'=>[
                        'class'=>'form-control',
                        'autocomplete' => 'off',
                        'placeholder' => 'enter your name here',
                        'maxlength' => 30
                        ]
                    ])
            ->add('telephone', TextType::class,
                [
                    // 'required'=> true,
                    'attr'=>[
                        'class'=>'form-control',
                        'autocomplete' => 'off',
                        'placeholder' => 'enter your name here',
                        'maxlength' => 30
                    ]
                ])
            ->add('image', FileType::class,
                [
                    'mapped' => false,
                    'required' => false,
                    'label' => 'Please upload a file'
                ])
            ->add('save', SubmitType::class,
                [
                    'attr' => [
                        'class' => 'btn btn-success'
                    ],
                    'label' => 'Create Trainer'
                ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trainer::class,
        ]);
    }
}
