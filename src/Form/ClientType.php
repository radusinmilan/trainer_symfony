<?php

namespace App\Form;

use App\Entity\Client;
use App\Entity\Trainer;
use App\Repository\TrainerRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    // 'required'=> true,
                    'attr'=>[
                        'class'=>'form-control',
                        'autocomplete' => 'off',
                        'placeholder' => 'enter your name here',
                        'maxlength' => 30
                    ]
                ])
            ->add('address', TextType::class,
                [
                    // 'required'=> true,
                    'attr'=>[
                        'class'=>'form-control',
                        'autocomplete' => 'off',
                        'placeholder' => 'enter your address here',
                        'maxlength' => 50
                        ]
                    ])
            ->add('email', TextType::class,
                [
                    // 'required'=> true,
                    'attr'=>[
                        'class'=>'form-control',
                        'autocomplete' => 'off',
                        'placeholder' => 'enter your email here',
                        'maxlength' => 20
                    ]
                ])
            ->add('telephone', TextType::class,
                [
                    // 'required'=> true,
                    'attr'=>[
                        'class'=>'form-control',
                        'autocomplete' => 'off',
                        'placeholder' => 'enter your telephone here',
                        'maxlength' => 20
                    ]
                ])
            ->add('trainer',  EntityType::class, [
                // looks for choices from this entity
                'attr'=>['class'=>'form-control'],
                'class' => Trainer::class,
                'choice_label' => 'name',
                'query_builder' => function(TrainerRepository $repo)
                {
                    return $repo->findByOrderAsc();
                }
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                 //'expanded' => true,
            ])
            ->add('save', SubmitType::class,
                [
                    'label' => 'Create Client',
                    'attr'=> ['class'=>'btn btn-success']
                ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
