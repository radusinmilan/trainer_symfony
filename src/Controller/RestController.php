<?php


namespace App\Controller;


use App\Entity\Client;
use App\Entity\Image;
use App\Entity\Trainer;
use App\Form\ImageType;
use App\Services\ClientService;
use App\Services\ImageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class RestController
 * @package App\Controller
 * @Route("/api", name="api.")
 */
class RestController extends AbstractController
{
    protected $clientService;
    protected $imageService;

    /**
     * RestController constructor.
     * @param ClientService $clientService
     * @param ImageService $imageService
     */
    public function __construct(ClientService $clientService, ImageService $imageService)
    {
        $this->clientService = $clientService;
        $this->imageService = $imageService;
    }


    /**
     * @Route("/trainer",  methods={"POST"})
     * @param Request $request
     */
    public function addTrainer(Request $request)
    {
        $trainer = new Trainer();
        $trainer->setName($request->get('name'));
        $file = $request->files->get('image');

        dd($file->guessExtension());
    }

    /**
     * @Route("/client",  methods={"POST"})
     * @param Request $request
     */
    public function addClient(Request $request)
    {
        $client = new Client();
        $client->setName($request->get('name'));
        //dd($request->request->all());
        $client->setAddress($request->get('address'));
        $client->setTelephone($request->get('telephone'));
        $client->setEmail($request->get('email'));
       // $client->setTrainer($request->get('trainer'));
       // dd($client);
        return $this->json($request->request->all());
    }

    /**
     * @Route("/client/all",  methods={"GET"})
     * @param Request $request
     */
    public function getClients(Request $request): Response
    {
        $clients = $this->clientService->findAll();
       // dd($clients);
       // dd($clients);
     /*   $arrayCollection = array();

        foreach($clients as $item) {
            $arrayCollection[] = array(
                'id' => $item->getId(),
                'name' => $item->getName(),
                'address' => $item->getAddress(),
                'telephone' => $item->getTelephone(),
                'email' => $item->getEmail(),
                'trainer'  => [
                     'id'  => $item->getTrainer()->getId(),
                     'name'=> $item->getTrainer()->getName(),
                     'telephone' => $item->getTrainer()->getTelephone(),
                     'image' => $item->getTrainer()->getImage()
                ]

            );
        }
        */
        /*
        $normalizers = [
          new ObjectNormalizer()
        ];

        $encoders = [
          new JsonEncoder()
        ];

        $serialzier = new Serializer($normalizers, $encoders);
        $data = $serialzier->serialize($clients, 'json');
        dd($data);
        */
        return $this->json($clients, 200, [], [
           ObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => function($object)
           {
               return $object->getId();
           }
        ]);

    }

    /**
     * @Route("/client/delete/{id}",  methods={"POST"})
     * @param $id
     * @throws \Exception
     */
    public function deleteClient($id)
    {
         return $this->clientService->delete($id);
    }


    /**
     * @Route("/image/upload",  methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadImage(Request $request)
    {
        //name
        $file = $request->files->get('name')->getClientOriginalName();
        $file = $request->files->get('name');
        //fileName
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $image = new Image();
        $image->setName($fileName);
        $this->imageService->saveImage($image);

        //extension
       // $file = $request->files->get('name')->guessExtension();
       // dd($fileName);
        $message = ['message'=> 'image added'];
        return $this->json($message, Response::HTTP_OK);

    }

}