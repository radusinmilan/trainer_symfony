<?php


namespace App\Controller;


use App\Entity\Client;
use App\Form\ClientType;
use App\Services\ClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ClientController
 * @package App\Controller
 * @Route("/client", name="client.")
 */
class ClientController extends AbstractController
{

    public $clientService;

    /**
     * ClientController constructor.
     * @param $clientService
     */
    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/index", name="index")
     */
    public function clients(Request $request): Response
    {
        $search = $request->query->get('search');
        $clients = $this->clientService->search($search);
        return $this->render('client/index.html.twig', [
            'clients' => $clients
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/add", name="add")
     */
    public function addClient(Request $request): Response
    {
        $client = new Client();

        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $this->clientService->save($client);

            $this->addFlash('success', 'Client created!');
            return $this->redirectToRoute('client.index');
        }
        return $this->render('client/client_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     * @param $id
     * @throws \Exception
     * @return Response
     */
    public function deleteClient($id): Response
    {
        $this->clientService->delete($id);
        $this->addFlash('success', 'Client deleted!');
        return $this->redirectToRoute('client.index');
    }

    /**
     * @Route("/update/{id}", name="update")
     * @param Request $request
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function updateClient(Request $request, $id): Response
    {
        // It can be the hole object of client where is id in parameters

        $client = $this->clientService->findById($id);
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $this->clientService->update($client, $id);
            $this->addFlash('success', 'Client updated!');
            return $this->redirectToRoute('client.index');
        }

        return $this->render('client/client_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws \Exception
     * @Route("/view/{id}", name="view")
     */
    public function viewClient($id): Response
    {
        $client = $this->clientService->findById($id);
        return $this->render('client/view.html.twig', [
            'client' => $client
        ]);
    }
}