<?php


namespace App\Controller;


use App\Entity\Trainer;
use App\Form\TrainerType;
use App\Services\TrainerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TrainerController
 * @package App\Controller
 * @Route("/trainer", name="trainer.")
 */
class TrainerController extends AbstractController
{

    public $trainerService;

    /**
     * TrainerController constructor.
     * @param $trainerService
     */
    public function __construct(TrainerService $trainerService)
    {
        $this->trainerService = $trainerService;
    }

    /**
     * @return Response
     * @Route("/welcome", name="welcome")
     */
    public function welcome(): Response
    {
        return $this->render('/welcome.html.twig');
    }

    /**
     * @return Response
     * @Route("/index", name="index")
     */
    public function traniers(): Response
    {
        $trainers = $this->trainerService->findAll();
        return $this->render('trainer/index.html.twig', [
            'trainers' => $trainers
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/add", name="add")
     */
    public function addTrainer(Request $request): Response
    {
        $trainer = new Trainer();
        $form = $this->createForm(TrainerType::class, $trainer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $request->files->get('trainer')['image'];
            $uploads_directory = $this->getParameter('uploads_directory');

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            dd($fileName);
            $trainer = $form->getData();
           // dd($trainer);
            // Create Trainer
            $this->trainerService->save($trainer);

            $this->addFlash('success', 'Trainer created!');
            return $this->redirectToRoute('trainer.index');
        }
        return $this->render('trainer/trainer_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete", name="delete")
     */
    public function deleteTrainer()
    {
        die;
    }
}